export enum FileTypes {
    File = 'file',
    Directory = 'directory',
    Root = 'root'
}

export interface FileNode {
    id: string;
    name?: string;
    children?: FileNode[];
    type: FileTypes;
}