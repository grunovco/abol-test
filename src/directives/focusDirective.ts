import { App, DirectiveBinding } from 'vue';

const focusDirective = {
    mounted(el: HTMLElement) {
        el.focus();
    },
    updated(el: HTMLElement) {
        el.focus();
    }
};

export default {
    install(app: App) {
        app.directive('focus', focusDirective);
    }
};