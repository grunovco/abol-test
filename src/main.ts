import { createApp } from 'vue';
import App from './App.vue';
import { createPinia } from 'pinia';
import focusDirective from './directives/focusDirective';
import '@/assets/reset.css';

const app = createApp(App);

// Создаем экземпляр Pinia
const pinia = createPinia();

// Регистрируем Pinia с Vue приложением
app.use(pinia);

app.use(focusDirective);

// Монтируем приложение
app.mount('#app');
