// stores/useFileStore.ts
import { defineStore } from 'pinia';
import { FileNode } from '../types';

export const useFileStore = defineStore('fileStore', {
    state: () => ({
        tree: <FileNode[]>[
            {
                id: '1',
                name: 'Dir 1',
                type: 'directory',
                children: [
                    {
                        id: '1-1',
                        name: 'Dir 1-1',
                        type: 'directory',
                        children: [
                            { id: '1-1-1', name: 'File 1-1-1', type: 'file' }
                        ]
                    },
                    { id: '1-2', name: 'File 1-2', type: 'file' }
                ]
            },
            {
                id: '2',
                name: 'Dir 2',
                type: 'directory',
                children: [
                    { id: '2-1', name: 'Dir 2-1', type: 'directory' },
                    { id: '2-2', name: 'File 2-2', type: 'file' }
                ]
            },
            { id: '3', name: 'File 2', type: 'file' }
        ]
    }),
    actions: {
        addNode(parentId: string, newNode: FileNode) {
            const parent = this.findNode(parentId);
            if (parent) {
                if (!parent.children) {
                    parent.children = [];
                }
                parent.children.push(newNode);
            } else {
                // Если parentId не найден, добавляем в корень дерева
                this.tree.push(newNode);
            }
        },
        removeNode(id: string) {
            const index = this.tree.findIndex(node => node.id === id);
            if (index !== -1) {
                this.tree.splice(index, 1);
            }
        },
        renameNode(id: string, newName: string) {
            const node = this.findNode(id);
            if (node) {
                node.name = newName;
            }
        },
        findNode(id: string, nodes?: FileNode[]): FileNode | undefined {
            // Если nodes не задан, используем this.tree
            const nodeList = nodes || this.tree;
            for (const node of nodeList) {
                if (node.id === id) {
                    return node;
                }
                if (node.children) {
                    const found = this.findNode(id, node.children);
                    if (found) {
                        return found;
                    }
                }
            }
            return undefined;
        }
    }
});
